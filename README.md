# eslint

```yaml
stages:
  - 🔎code-quality

include:
  - project: 'simplify-devops/eslint'
    file: 'eslint.gitlab-ci.yml'

#-----------------------------------------------------------------------------------------
# Quality
#-----------------------------------------------------------------------------------------
🔎:code:quality:eslint:
  stage: 🔎code-quality
  extends: .eslint:analyzer
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
    - if: $CI_MERGE_REQUEST_IID

```